//
//  BasicViewController.m
//  QiaoGu
//
//  Created by JackLiu on 14-8-11.
//  Copyright (c) 2014年 ZXInsight. All rights reserved.
//
#import "BasicViewController.h"
#import "AppDelegate.h"
//navigationItem.titleView的最大宽度为188，若超过会自动裁剪为188
#define nav_mid_view_width 188
#import "MBProgressHUD.h"
#import "AFNetworking.h"
#import "JSONKit.h"
@interface BasicViewController ()<MBProgressHUDDelegate,UIImagePickerControllerDelegate,UIActionSheetDelegate,UIGestureRecognizerDelegate>
{
   
}
@property(nonatomic,strong) AFHTTPRequestOperationManager *bClient;
@end

@implementation BasicViewController

@synthesize topNavView = _topNavView;
@synthesize leftButton = _leftButton;
@synthesize rightButton = _rightButton;
@synthesize titleLabel = titleLabel;

@synthesize isEnterForeground = _isEnterForeground;
@synthesize HUD;

-(instancetype)init
{
    self = [super init];
    if (self) {
        // [self.view setBackgroundColor:[UIColor whiteColor]];
        // Do any additional setup after loading the view.
        if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
        {
            [self setEdgesForExtendedLayout:UIRectEdgeNone];
        }
        //    self.navigationController.navigationBarHidden=YES;
        //    UIView *top=[[UIView alloc] initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 64)];
        //    [top setBackgroundColor:[UIColor whiteColor]];
        //    [self.view addSubview:top];
        
        
        //  self.navigationItem.hidesBackButton = YES;
        [self.navigationController.navigationBar setTintColor:[UIColor clearColor]];
        _topNavView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH-132, 44)];
        _topNavView.backgroundColor = [UIColor clearColor];
        self.navigationItem.titleView = _topNavView;
        
        titleLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH-132, 44)];
        titleLabel.font = [UIFont fontWithName:@"Avenir-Black" size:20];
        //titleLabel.textColor = [UIColor whiteColor];
        titleLabel.backgroundColor = [UIColor clearColor];
        titleLabel.textAlignment = NSTextAlignmentCenter;
        [titleLabel setTextColor:Color(@"3c3c3c")];
        [self.navigationItem.titleView addSubview:titleLabel];
        
        _leftButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_leftButton setFrame:CGRectMake(0, 20, 44, 44)];
        //  _leftButton.imageEdgeInsets = UIEdgeInsetsMake(0,-20, 0, 14);
        
        [_leftButton addTarget:self action:@selector(leftButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        _rightButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_rightButton setFrame:CGRectMake(SCREEN_WIDTH-44, 20, 44, 44)];
        _rightButton.imageEdgeInsets = UIEdgeInsetsMake(0, 5, 0, -5);
        [_rightButton addTarget:self action:@selector(rightButtonAction) forControlEvents:UIControlEventTouchUpInside];
        
        [_rightButton setTitleColor:[AciMath getColor:@"787878"] forState:UIControlStateNormal];
        UITapGestureRecognizer *viewTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(topNavTapAction)];
        [_topNavView addGestureRecognizer:viewTap];
        [viewTap setNumberOfTapsRequired:1];
        [_leftButton setImage:[UIImage imageNamed:@"bt_return"] forState:UIControlStateNormal];
        //处理按钮偏右的问题
        UIBarButtonItem *backNavigationItem = [[UIBarButtonItem alloc] initWithCustomView:_leftButton];
        UIBarButtonItem *negativeSpacer = [[UIBarButtonItem alloc]
                                           initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace
                                           target:nil action:nil];
        negativeSpacer.width = -10;
        self.navigationItem.leftBarButtonItems = @[negativeSpacer, backNavigationItem];
        
        UIBarButtonItem *rightItem=[[UIBarButtonItem alloc] initWithCustomView:_rightButton];
        self.navigationItem.rightBarButtonItems =  @[negativeSpacer, rightItem];
        // [top addSubview:_leftButton];
        //[top addSubview:_rightButton];
        self.appdelegate = (AppDelegate *)[[UIApplication sharedApplication] delegate];

    }
    return  self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
   }

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    if(IOS7){
        
        self.navigationController.interactivePopGestureRecognizer.delegate = self;
        
    }
    _isEnterForeground = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    _isEnterForeground = NO;
}
-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    self.navigationController.delegate = nil;
}
- (void)leftButtonAction
{
    [self.navigationController popViewControllerAnimated:YES];

}
-(void)rightButtonAction
{
    
}
- (void)topNavTapAction
{

}
-(void)ShowHUD
{
    if (HUD) {
        [self HideHUD];
    }
    HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.labelText=@"加载中";
    HUD.delegate=self;
}
-(void)showCustom:(UIView *)view
{
    if (HUD) {
        [self HideHUD];
    }
    HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    HUD.mode=MBProgressHUDModeCustomView;
    HUD.userInteractionEnabled=NO;
  //  HUD.dimBackground=YES;
    HUD.customView=view;
    //[HUD show:YES];
}
-(void)ShowHUD:(NSString *)title
{
    if (HUD) {
        if (HUD.hidden) {
            HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.delegate=self;
        }
    }
    else
    {
        HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.delegate=self;
    }
    HUD.labelText=title;
}
-(void)HideHUD
{
    [HUD hide:YES];
}
-(void)hideKayBoard
{
     [[UIApplication sharedApplication] sendAction:@selector(resignFirstResponder) to:nil from:nil forEvent:nil];
}
-(void)HideHUDAfter2S:(NSString *)desc
{
    HUD.labelText =desc;
    HUD.mode = MBProgressHUDModeCustomView;
    [HUD hide:YES afterDelay:2];
  
    
}
-(void)ShowHUDin2s:(NSString *)msg complete:(SEL)complete
{
    if (HUD) {
        if (HUD.hidden) {
            HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            HUD.delegate=self;
        }
    }
    else
    {
        HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.delegate=self;
    }
    HUD.labelText =msg;
    HUD.mode = MBProgressHUDModeCustomView;
    [HUD hide:YES afterDelay:2];
    [self performSelector:complete withObject:nil afterDelay:2];
     //RunBlockAfterDelay(2, complete);
}
void RunBlockAfterDelay(NSTimeInterval delay, void (^block)(void))
{
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, NSEC_PER_SEC*delay),
                  dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH,0), block);
}

-(void)ShowHUDin2S:(NSString *)msg
{
    if (HUD) {
        if (HUD.hidden) {
            HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            HUD.delegate=self;
        }
    }
    else
    {
        HUD =[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        HUD.delegate=self;
    }
    [self HideHUDAfter2S:msg];
}
-(void)hudWasHidden:(MBProgressHUD *)hud
{
    [HUD removeFromSuperview];
    HUD=nil;

}

- (void)setTitle:(NSString *)title
{
    titleLabel.text = title;
}
-(AFHTTPRequestOperationManager *)bClient
{
    if (!_bClient) {
        _bClient=[AFHTTPRequestOperationManager manager];
        [_bClient.requestSerializer willChangeValueForKey:@"timeoutInterval"];
        _bClient.requestSerializer.timeoutInterval = 10.f;
        [_bClient.requestSerializer didChangeValueForKey:@"timeoutInterval"];
        
    }
    return _bClient;
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)Get:(NSString *)url complete:(void (^)(NSDictionary *arr))complete
{
    [self ShowHUD];
 
    _bClient.requestSerializer = [AFHTTPRequestSerializer serializer];
    [self.bClient GET:url parameters:nil success:^(AFHTTPRequestOperation *operation,id responseObject)
     {
         [self HideHUD];
         NSDictionary *dic =[responseObject objectFromJSONData];
         if ([dic isKindOfClass:[NSDictionary class]])
         {
             
             if (dic[@"result"]) {
                 if ([dic[@"result"] isEqualToString:@"1"]) {
                     [self HideHUD];
                     complete(dic);
                 }
                 else
                 {
                     [self HideHUDAfter2S:dic[@"message"]];
                 }
                 
             }
             else
             {
                 [self HideHUDAfter2S:@"服务器错误!"];
             }
         }
         
     }failure:^(AFHTTPRequestOperation *operation,NSError *error){
         //
         NSLog(@"err:%@",error.description);
         [self HideHUDAfter2S:error.description];
     }];
    
}
-(void)Post:(NSString *)url parameters:(NSDictionary *)parameters complete:(void (^)(NSDictionary *dic))complete
{
    [self ShowHUD];
 
    [self.bClient POST:url parameters:parameters success:^(AFHTTPRequestOperation *operation,id responseObject)
     {
         NSDictionary *dics =responseObject;
         if ([dics isKindOfClass:[NSDictionary class]])
         {
             if (dics[@"result"]) {
                [self HideHUDAfter2S:@"操作成功"];
                 if ([dics[@"result"] isEqualToString:@"1"]) {
                   
                     complete(dics);
                    
                 }
                 
             }
             else
             {
                 [self HideHUDAfter2S:@"服务器错误!"];
             }
         }
         else
         {
             [self HideHUD];
             NSLog(@"responseObject:%@",[[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding]);
         }
         
     }failure:^(AFHTTPRequestOperation *operation,NSError *error){
         //
         [self HideHUDAfter2S:error.description];
     }];
    
}
-(void)postData:(NSString *)url data:(NSData *)data param:(NSDictionary *)param complete:(void(^)(NSDictionary *dic))complete
{
 
    [self.bClient POST:[NSString APIURLString:@"creatgather.ashx"] parameters:param constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"data" fileName:[NSString stringWithFormat:@""] mimeType:@"application/json"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
     
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        [self HideHUD];
    }];

}
-(void)PostJPG:(NSString *)url data:(NSData *)data parameters:(NSDictionary *)parameters complete:(void (^)(NSDictionary *))complete
{
    [self ShowHUD];
 
    [self.bClient POST:url parameters:parameters constructingBodyWithBlock:^(id<AFMultipartFormData> formData) {
        [formData appendPartWithFileData:data name:@"file" fileName:[NSString stringWithFormat:@"message_%@.jpg",@""] mimeType:@"image/jpg"];
    } success:^(AFHTTPRequestOperation *operation, id responseObject) {
        
        NSDictionary *dics =responseObject;
        if ([dics isKindOfClass:[NSDictionary class]])
        {
            if (dics[@"result"]) {
                [self HideHUD];
                if ([dics[@"result"] isEqualToString:@"1"]) {
                    complete(dics);
                }
                else
                {
                    [self HideHUDAfter2S:dics[@"message"]];
                }
                
            }
            else
            {
                [self HideHUDAfter2S:@"服务器错误!"];
            }
        }
        else
        {
             [self HideHUDAfter2S:@"服务器错误!"];
        }

    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self HideHUDAfter2S:error.description];
    }];

    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
