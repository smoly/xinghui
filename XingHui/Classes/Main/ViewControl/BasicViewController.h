//
//  BasicViewController.h
//  QiaoGu
//
//  Created by JackLiu on 14-8-11.
//  Copyright (c) 2014年 ZXInsight. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MBProgressHUD.h"
@interface BasicViewController : UIViewController

@property (nonatomic,strong)UIView *topNavView;
@property (nonatomic,strong)UILabel *titleLabel;
@property (nonatomic,strong)UIButton *leftButton;
@property (nonatomic,strong)UIButton *rightButton;
@property(nonatomic,strong)AppDelegate *appdelegate;
@property(nonatomic,strong)MBProgressHUD *HUD;
@property (nonatomic)bool isEnterForeground;
-(void)ShowHUDin2S:(NSString *)msg;
- (void)setTitle:(NSString *)title;
- (void)leftButtonAction;
- (void)rightButtonAction;
- (void)topNavTapAction;
//- (void)viewTapAction;
-(void)ShowHUD;
-(void)HideHUD;
-(void)showCustom:(UIView *)view;//显示自定义UIView
-(void)HideHUDAfter2S:(NSString *)desc;
-(void)ShowHUDin2s:(NSString *)msg complete:(SEL)complete;
-(void)hideKayBoard;
-(void)ShowHUD:(NSString *)title;
-(void)Get:(NSString *)url complete:(void (^)(NSDictionary *arr))complete;
-(void)Post:(NSString *)url parameters:(NSDictionary *)parameters complete:(void (^)(NSDictionary *arr))complete;
-(void)PostJPG:(NSString *)url data:(NSData *)data parameters:(NSDictionary *)parameters complete:(void (^)(NSDictionary *))complete;
@end
